import argparse
from pathlib import Path

from matplotlib import pyplot as plt

import chart_helpers
import chart_source_helpers


def get_args():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    chart_source_helpers.add_arguments(parser)
    parser.add_argument(
        '-f',
        '--first-index',
        default=None,
        help='first index, must be larger than or equal to <ma2>',
    )
    parser.add_argument(
        '-N',
        '--num-of-charts',
        type=int,
        default=1,
        help='number of conjecutive charts from <first index> to draw',
    )
    parser.add_argument(
        '-n',
        '--num-of-candles',
        type=int,
        default=40,
        help='number of candles to draw',
    )
    parser.add_argument(
        '--ma1',
        type=int,
        default=60,
        help='short term moving average',
    )
    parser.add_argument(
        '--ma2',
        type=int,
        default=200,
        help='long term moving average',
    )
    parser.add_argument(
        '-t',
        '--type',
        default='candlestick_bw',
        help='chart type',
    )
    parser.add_argument(
        '-H',
        '--height',
        type=int,
        default=-1,
        help='chart height, same value as <chart width> if -1',
    )
    return parser.parse_args()


def draw_chart(
    df0,
    out,
    type='line',
    height=-1,
    mas=[],
):
    try:
        df = df0.reset_index()
        width = len(df) // 5
        if height == -1:
            height = width
        figsize = (width, width)
        fig, (ax, ax2) = plt.subplots(2,
                                      1,
                                      figsize=figsize,
                                      sharex=True,
                                      gridspec_kw={'height_ratios': [4, 1]})
        if type == 'line':
            ax.plot(df.index, df['Close'], linewidth=10)
        elif type == 'ohlc':
            chart_helpers.draw_ohlc(df, ax)
        elif type == 'candlestick':
            chart_helpers.draw_candle(df, ax)
        elif type == 'candlestick_bw':
            chart_helpers.draw_candle(df, ax, '#aaa', '#000')
        else:
            raise Exception(f'Unknow chart type: {type}')
        chart_helpers.draw_moving_averages(ax, df, mas)
        ax.set_axis_off()
        chart_helpers.draw_volume(ax2, df)
        ax2.set_axis_off()
        plt.tight_layout()
        Path(out).parent.mkdir(exist_ok=True, parents=True)
        plt.savefig(out)
    finally:
        plt.close()


def main():
    args = get_args()
    print(args)
    if args.ma1 >= args.ma2:
        raise Exception(f'ma2 must be larger than ma1')
    first_ = args.first_index
    df = None
    if first_ is not None:
        if '-' in first_:
            df, interval = chart_source_helpers.load(args)
            first = len(df) - len(df[df.index >= first_])
            print(first)
        else:
            first = int(first_)
    else:
        first = args.ma2
    if first < args.ma2:
        raise Exception(
            f'start_index must be larger than or equal to {args.ma2}')
    if df is None:
        df, interval = chart_source_helpers.load(args)
    df['MA1'] = df['Close'].rolling(args.ma1).mean()
    df['MA2'] = df['Close'].rolling(args.ma2).mean()
    df['VMA'] = df['Volume'].rolling(args.ma2).mean()
    root = Path(
        'out'
    ) / 'charts' / f'draw_chart,{args.num_of_candles}' / args.source / args.ticker / interval
    i = 0
    while True:
        if args.num_of_charts != -1 and i >= args.num_of_charts:
            break
        name = first + i
        if name + args.num_of_candles >= len(df):
            break
        df1 = df[name:name + args.num_of_candles]
        i = i + 1
        fname = root / f'{df1.index[-1].strftime("%Y-%m-%d-%H-%M-%S")}.png'
        draw_chart(df1,
                   fname,
                   type=args.type,
                   height=args.height,
                   mas=[{
                       'name': 'MA1',
                       'color': 'k',
                       'linewidth': 16,
                       'linestyle': '--',
                       'alpha': 0.125,
                   }, {
                       'name': 'MA2',
                       'color': 'k',
                       'linewidth': 16,
                       'linestyle': '-',
                       'alpha': 0.125,
                   }])
        chart_helpers.log(name, interval, df1, args)


main()
