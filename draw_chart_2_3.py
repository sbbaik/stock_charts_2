import argparse
from pathlib import Path

from matplotlib import pyplot as plt

import chart_helpers
import chart_source_helpers


def get_args():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    chart_source_helpers.add_arguments(parser)
    parser.add_argument(
        '-f',
        '--first-index',
        default=None,
        help='first index, must be larger than or equal to <duration>',
    )
    parser.add_argument(
        '-N',
        '--num-of-charts',
        type=int,
        default=1,
        help='number of conjecutive charts from <first index> to draw',
    )
    parser.add_argument(
        '-n',
        '--num-of-candles',
        type=int,
        default=20,
        help='number of candles to draw',
    )
    parser.add_argument(
        '--duration',
        type=int,
        default=200,
        help='chart data duration',
    )
    return parser.parse_args()


def chart(df0, out, num_of_candles):
    try:
        df = df0.reset_index()
        width = num_of_candles * len(df) // 1000
        figsize = (width, width)
        fig, (ax1, ax2,
              ax3) = plt.subplots(3,
                                  1,
                                  figsize=figsize,
                                  gridspec_kw={'height_ratios': [2, 1, 1]})
        df_ = df[-num_of_candles:]
        chart_helpers.draw_candle(df_, ax1, '#aaa', '#000')
        ax1.margins(0)
        ax1.set_axis_off()
        ax2.plot(df.index, df['Close'], linewidth=3, color='k')
        chart_helpers.draw_moving_averages(ax2, df, [
            {
                'name': 'MA',
                'color': 'k',
                'linewidth': 5,
                'linestyle': '--',
                'alpha': 0.25,
            },
            {
                'name': 'MA20',
                'color': 'k',
                'linewidth': 5,
                'linestyle': '-',
                'alpha': 0.25,
            },
        ])
        ax2.axvline(len(df) - num_of_candles,
                    linewidth=1,
                    alpha=0.25,
                    color='k')
        ax2.margins(0)
        ax2.set_axis_off()
        ax3.plot(df.index, df['VV'], linewidth=3, color='k')
        lo, hi = ax3.get_ylim()
        d = hi - lo
        v = max(df['VV'])
        p1 = lo + 0.2 * d
        p2 = lo + 0.4 * d
        p3 = lo + 0.6 * d
        p4 = lo + 0.8 * d
        if v < 0.1:
            ax3.axhline(p1, linewidth=5, alpha=0.25, color='k')
            ax3.axhline(p2, linewidth=1, alpha=0.25, color='k')
            ax3.axhline(p3, linewidth=1, alpha=0.25, color='k')
            ax3.axhline(p4, linewidth=1, alpha=0.25, color='k')
        elif v < 0.3:
            ax3.axhline(p1, linewidth=5, alpha=0.25, color='k')
            ax3.axhline(p2, linewidth=5, alpha=0.25, color='k')
            ax3.axhline(p3, linewidth=1, alpha=0.25, color='k')
            ax3.axhline(p4, linewidth=1, alpha=0.25, color='k')
        elif v < 0.5:
            ax3.axhline(p1, linewidth=5, alpha=0.25, color='k')
            ax3.axhline(p2, linewidth=5, alpha=0.25, color='k')
            ax3.axhline(p3, linewidth=5, alpha=0.25, color='k')
            ax3.axhline(p4, linewidth=1, alpha=0.25, color='k')
        else:
            ax3.axhline(p1, linewidth=5, alpha=0.25, color='k')
            ax3.axhline(p2, linewidth=5, alpha=0.25, color='k')
            ax3.axhline(p3, linewidth=5, alpha=0.25, color='k')
            ax3.axhline(p4, linewidth=5, alpha=0.25, color='k')
        ax3.axvline(len(df) - num_of_candles,
                    linewidth=1,
                    alpha=0.25,
                    color='k')
        ax3.margins(0)
        ax3.set_axis_off()
        plt.tight_layout()
        Path(out).parent.mkdir(exist_ok=True, parents=True)
        plt.savefig(out)
    finally:
        plt.close()


def main():
    args = get_args()
    print(args)
    first_ = args.first_index
    df = None
    if first_ is not None:
        if '-' in first_:
            df, interval = chart_source_helpers.load(args)
            first = len(df) - len(df[df.index >= first_])
            print(first)
        else:
            first = int(first_)
    else:
        first = args.duration
    if first < args.duration:
        raise Exception(
            f'start_index must be larger than or equal to {args.duration}')
    if df is None:
        df, interval = chart_source_helpers.load(args)
    df['MA'] = df['Close'].rolling(args.duration).mean()
    df['MA20'] = df['Close'].rolling(20).mean()
    df['V'] = (df['High'] - df['Low']) / df['Low']
    df['VV'] = df['V'].rolling(20).mean()
    print(df)
    root = Path(
        'out'
    ) / 'charts' / f'draw_chart_2_3,{args.duration},{args.num_of_candles}' / args.source / args.ticker / interval
    i = 0
    while True:
        if args.num_of_charts != -1 and i >= args.num_of_charts:
            break
        name = first + i
        if name + args.duration >= len(df):
            break
        df1 = df[name:name + args.duration]
        i = i + 1
        fname = root / f'{df1.index[-1].strftime("%Y-%m-%d-%H-%M-%S")}.png'
        chart(df1, fname, args.num_of_candles)
        chart_helpers.log(name, interval, df1, args)


main()
