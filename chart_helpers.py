import matplotlib.patches as mpatches


def gray(c):
    return (c, c, c)


def draw_volume(ax, df):
    df['Volume'].plot(kind='bar',
                      ax=ax,
                      label='Volume',
                      color=gray(0.5),
                      width=0.8,
                      alpha=1)
    df['VMA'].plot(kind='area',
                   ax=ax,
                   label='Volume',
                   color=gray(0.25),
                   lw=0,
                   alpha=0.25)


def draw_candle(df, ax, upcolor='#c70', downcolor='#37e'):
    for x_, val in df.iterrows():
        o = val['Open']
        h = val['High']
        l = val['Low']
        c = val['Close']
        if val['Volume'] > 0:
            if c < o:
                t = o
                b = c
                color = downcolor
            else:
                t = c
                b = o
                color = upcolor
            ax.plot([x_, x_], [h, t], color=color, lw=4)
            ax.plot([x_, x_], [b, l], color=color, lw=4)
            ax.add_patch(
                mpatches.Rectangle((x_ - 0.35, b),
                                   0.7,
                                   t - b,
                                   linewidth=2,
                                   color=color))
        else:
            ax.plot([x_ - 0.35, x_ + 0.35], [c, c], color='r', lw=2)


def draw_ohlc(df, ax):
    for x_, val in df.iterrows():
        o = val['Open']
        h = val['High']
        l = val['Low']
        c = val['Close']
        if val['Volume'] > 0:
            ax.plot([x_, x_], [l, h], color='k', lw=6)
            ax.plot([x_, x_ - 0.25], [o, o], color='k', lw=6)
            ax.plot([x_, x_ + 0.25], [c, c], color='k', lw=6)
        else:
            ax.plot([x_ - 0.25, x_ + 0.25], [c, c], color='k', lw=6)


def draw_moving_averages(ax, df, mas):
    for ma in mas:
        ax.plot(
            df.index,
            df[ma['name']],
            color=ma['color'],
            linewidth=ma['linewidth'],
            linestyle=ma['linestyle'],
            alpha=ma['alpha'],
        )


def log(name, interval, df1, args):
    xX = df1.index.max()
    xN = df1.index.min()
    str = f'{args.source}|{interval}|{args.ticker}|{args.num_of_candles}|{name}|{xN}|{xX}'
    print(str)
