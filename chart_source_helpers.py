from datetime import datetime
from pathlib import Path

import pandas as pd
import pybithumb
import pyupbit
from pykrx import stock

import ticker_name_dict_3


def cache_folder():
    return Path('..') / 'cache'


def load(args):
    source = args.source
    if source == 'bithumb':
        return load_bithumb(args)
    if source == 'upbit':
        return load_upbit(args)
    if source == 'krx':
        return load_krx(args)
    raise Exception(f'Unknown source: {source}')


def load_bithumb(args):
    ticker = args.ticker
    interval = args.interval
    if interval is None:
        interval = '24h'
    df = pybithumb.get_candlestick(ticker, chart_intervals=interval)
    df = df.reset_index()
    df = df.rename(
        columns={
            'time': 'Time',
            'open': 'Open',
            'high': 'High',
            'low': 'Low',
            'close': 'Close',
            'volume': 'Volume',
        })
    df = df.set_index('Time')
    return df, interval


def load_upbit(args):
    ticker = args.ticker
    interval = args.interval
    reload = args.reload
    if interval is None:
        interval = 'day'
    fname = cache_folder() / 'upbit' / interval / f'{ticker}.csv'
    if not reload and fname.is_file():
        df_cached = pd.read_csv(fname)
        df_cached['Time'] = pd.to_datetime(df_cached['Time'])
        return df_cached.set_index('Time'), interval
    if fname.is_file():
        df_cached = pd.read_csv(fname)
        df_cached['Time'] = pd.to_datetime(df_cached['Time'])
        fromDateTime = df_cached['Time'].max()
    else:
        fromDateTime = None
    df = pyupbit.get_ohlcv_from(ticker,
                                fromDatetime=fromDateTime,
                                interval=interval)
    if df is not None:
        df = df.reset_index().rename(
            columns={
                'index': 'Time',
                'open': 'Open',
                'high': 'High',
                'low': 'Low',
                'close': 'Close',
                'volume': 'Volume',
                'value': 'Value',
            })
    if fromDateTime is not None:
        df = pd.concat([df_cached[:-1], df])
    df = df.set_index('Time')
    fname.parent.mkdir(exist_ok=True, parents=True)
    df.to_csv(fname)
    return df, interval


def load_krx_(ticker, fromdate, todate):
    r = ticker_name_dict_3.find(ticker)
    print(r)
    type = r['type']
    if type == 'etf':
        df = stock.get_etf_ohlcv_by_date(fromdate, todate, ticker)
    elif type == 'etn':
        raise Exception(f'etn data not supported')
    else:
        df = stock.get_market_ohlcv_by_date(fromdate,
                                            todate,
                                            ticker,
                                            adjusted=True)
    return df.reset_index().rename(
        columns={
            '날짜': 'Time',
            '시가': 'Open',
            '고가': 'High',
            '저가': 'Low',
            '종가': 'Close',
            '거래량': 'Volume',
        })


def load_krx(args):
    ticker = args.ticker
    reload = args.reload
    todate = datetime.now().strftime('%Y%m%d')
    fname = cache_folder() / 'krx' / f'{ticker}.csv'
    if not reload and fname.is_file():
        df1 = pd.read_csv(fname)
        df1['Time'] = pd.to_datetime(df1['Time'])
        return df1.set_index('Time'), 'day'
    if fname.is_file():
        df1 = pd.read_csv(fname)
        df1['Time'] = pd.to_datetime(df1['Time'])
        fromdate = df1['Time'].max().strftime('%Y%m%d')
        df1 = df1[:-1]
        df2 = load_krx_(ticker, fromdate, todate)
        df2['Time'] = pd.to_datetime(df2['Time'])
        df = pd.concat([df1, df2]).set_index('Time')
        df.to_csv(fname)
        return df, 'day'
    else:
        fromdate = '19800101'
        df = load_krx_(ticker, fromdate, todate).set_index('Time')
        fname.parent.mkdir(exist_ok=True, parents=True)
        df.to_csv(fname)
        return df, 'day'


def add_arguments(parser):
    parser.add_argument(
        'ticker',
        help='symbol name',
    )
    parser.add_argument(
        '-s',
        '--source',
        default='bithumb',
        help='chart data source',
    )
    parser.add_argument(
        '-i',
        '--interval',
        default=None,
        help='chart time frame interval, "day" if None',
    )
    parser.add_argument(
        '-r',
        '--reload',
        action='store_true',
        help='reload data from chart source',
    )


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    add_arguments(parser)
    args = parser.parse_args()
    print(args)
    df = load(args)
    print(df)