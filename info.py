import argparse
from pathlib import Path


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('dir')
    return parser.parse_args()


args = get_args()
dir = Path(args.dir)
for fname in dir.glob('*.png.info'):
    with open(fname) as f:
        content = f.read().strip()
    cs = content.split('|')
    if len(cs) != 4:
        continue
    lo = float(cs[2])
    hi = float(cs[3])
    index = Path(fname.stem).stem
    m1 = '-' if lo < -0.05 else ' '
    m2 = '+' if hi > 0.1 else ' '
    print(f'{index}|{cs[0]}|{cs[1]}|{lo:8.2f}|{hi:8.2f}|{m1}|{m2}')
